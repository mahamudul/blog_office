-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2017 at 05:27 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `name`) VALUES
(1, 'JAVA'),
(2, 'PHP'),
(3, 'HTML'),
(4, 'CSS');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_post`
--

CREATE TABLE `tbl_post` (
  `id` int(11) NOT NULL,
  `cat` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `author` varchar(50) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_post`
--

INSERT INTO `tbl_post` (`id`, `cat`, `title`, `body`, `image`, `author`, `tags`, `date`) VALUES
(1, 1, 'Our post title here - java', '<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>', 'post2.png', 'Deloar', 'java, programming', '2017-05-19 17:58:18'),
(2, 2, 'Our post title here - php', '<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>', 'post1.png', 'Deloar', 'php ,programming', '2017-05-19 17:58:18'),
(3, 3, 'HTML post title here', '<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\r\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\r\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>', 'post2.png', 'Admin', 'html, html5', '2017-05-19 18:02:57'),
(4, 4, 'CSS post title here', '<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\r\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\r\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>', 'post1.png', 'Admin', 'css, css3', '2017-05-19 18:02:57'),
(5, 1, 'bootstrap post title', '<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\r\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\r\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>', 'post2.png', 'Admin', 'bootstrap', '2017-05-19 18:07:33'),
(6, 2, 'Our post title here oop', '<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\r\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>\r\n<p>When it comes to building computer programming skills, learning Java will take you way beyond the games of your childhood. Knowing Java will be a clear advantage in your future career.\r\nWhen you learn Java, you’re learning more than just a language. Because Java is a technology platform with many interconnected capabilities that can give cutting-edge, in-demand job skills. In fact, Java tops Inc.com’s list of the ten most popular programming languages. Java can take you where you want to go.</p>', 'post1.png', 'Admin', 'oop, c++', '2017-05-19 18:07:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_post`
--
ALTER TABLE `tbl_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbl_post`
--
ALTER TABLE `tbl_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
